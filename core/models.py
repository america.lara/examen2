from django.db import models

from django.contrib.auth.models import User

# Create your models here.

##STADIUM

class Stadium(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    gg_title = models.CharField(max_length=128, default="Stadium Title")
    desciption = models.CharField(max_length=256, default="Stadium Description")

    def __str__(self):
        return self.gg_title
    
##TEAM

class Team(models.Model):
    name1 = models.CharField(max_length=128, default="team name")
    desciption2 = models.CharField(max_length=256, default="team Description")

    def __str__(self):
        return self.name1

##CITY

##TEAM

class City(models.Model):
    name2 = models.CharField(max_length=128, default="city name")

    def __str__(self):
        return self.name2